### NLP ###

* Eos.prl classifies if a period (".") in a given text is an EOS or just an abbreviation marker.
* Make_block.prl classifies the type of line in a plaintext email (i.e. table, header, metadata, body)

### Run ###

* perl eos.prl < sent.data.train | cat
* perl make_block.prl < segment.data.train | cat
